/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inverciondependencia;

/**
 *
 * @author CompuStore
 */
public class Computadora implements Programar{
    private String Tipo;
    
    public Computadora(String Tipo) {
        this.Tipo = Tipo;
    }
    
    @Override
    public void programar() {
        System.out.println("Esta Programando en" + " " + Tipo);
    } 
}
